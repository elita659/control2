package com.epam.cont;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class WorkingTest {
    @Test
    void regularTest(){
        int array[] = {6,32,9,16,7};
        Working working = new Working();

        working.work(array);

        assertArrayEquals(new int[]{6,7,9,16,32}, array);
    }
}